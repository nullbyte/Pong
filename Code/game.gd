extends Node2D

var score = 0
var rippledata = load("res://Scenes/ripple.tscn")

func _ready():
	events.connect("hitPaddle", self, "increaseScore")
	events.connect("hitPaddle", self, "showCollideEffectsLeft")
	events.connect("hitWall", self, "showCollideEffectsRight")
	events.connect("gameOver", self, "onGameOver")
	events.connect("startGame", self, "start")
	events.emit_signal("startGame")

func start():
	score = 0
	get_tree().paused = false

func increaseScore():
	score += 1
	$UI.get_node("Score").text = "Score: " + str(score)

func showCollideEffectsLeft():
	var ripple = rippledata.instance()
	add_child(ripple)
	move_child(ripple, 0)
	ripple.position.y = $Ball.position.y
	ripple.position.x = $Ball.position.x - 30
	
	$Anim.play("BumpLeft")
	$Background/GradientAnimation.play("Left")

func showCollideEffectsRight():
	$Anim.play("BumpRight")

func onGameOver():
	if config.readString("Settings", "BallSpeed", "100") == "100" and score > int(str(config.readString("Scores", "HighScore"))):
		config.writeString("Scores", "HighScore", str(score))
	get_tree().paused = true

var relative = config.readString("Settings", "RelativeMovement", "False") == "True"

func _input(event):
	if relative:
		if event is InputEventScreenDrag:
			if $Paddle.position.y - ($Paddle/Collider.shape.extents.y) + event.relative.y * ($Paddle.speed / 500) > 0 and $Paddle.position.y + ($Paddle/Collider.shape.extents.y / 2) + event.relative.y * ($Paddle.speed / 500) < $Bottom.position.y:
				$Paddle.position.y += event.relative.y * ($Paddle.speed / 500)
	else:
		if event is InputEventScreenTouch or event is InputEventScreenDrag:
			if event.position.y - $Paddle/Collider.shape.extents.y > 0 and event.position.y + $Paddle/Collider.shape.extents.y < get_viewport_rect().size.y:
				$Paddle.position.y = event.position.y
	$Paddle.position.x = 0
