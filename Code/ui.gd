extends Control

func _ready():
	events.connect("hitPaddle", self, "updateScore")
	events.connect("startGame", self, "updateScore")

func updateScore():
	$Score.text = "Score: " + str(get_parent().score)

func pauseGame():
	events.emit_signal("pauseGame")
