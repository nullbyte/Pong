extends Node

var conf = ConfigFile.new()
var file = "user://pong.cfg"

func _ready():
	conf.load(file)

func writeString(section: String, name: String, value: String):
	conf.set_value(section, name, value)
	conf.save(file)

func readString(section: String, name: String, default: String = ""):
	var result = conf.get_value(section, name)
	if result == null:
		return default
	else:
		return result
