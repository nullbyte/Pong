extends KinematicBody2D

var speed = int(config.readString("Settings", "PaddleSpeed", "50")) * 15

func _ready():
	events.connect("startGame", self, "start")

func start():
	yield(get_tree(), "idle_frame")
	position.y = get_viewport_rect().size.y / 2

func _physics_process(delta):
	var velocity = Vector2.ZERO
	if Input.is_action_pressed("ui_up"):
		velocity.y -= speed
	elif Input.is_action_pressed("ui_down"):
		velocity.y += speed
	move_and_collide(velocity * delta)

