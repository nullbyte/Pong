extends Control

var action = 0

func _ready():
	visible = false
	events.connect("gameOver", self, "showGameOver")

func showGameOver():
	visible = true
	$AnimationPlayer.play("SlideUp")
	$Score.text = str(get_node("/root/Game").score)
	$HighScore.text = config.readString("Scores", "HighScore", "0")

func _on_animation_finished(anim_name):
	if anim_name == "SlideDown":
		visible = false
		if action == 0:
			events.emit_signal("startGame")
		elif action == 1:
			get_tree().change_scene("res://Scenes/mainmenu.tscn")

func gotoMenu():
	action = 1
	$AnimationPlayer.play("SlideDown")

func restartGame():
	action = 0
	$AnimationPlayer.play("SlideDown")
