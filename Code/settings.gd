extends Control

func _ready():
	readSettings()
	applySettings()
	updateWarning()

func readSettings():
	$ColorRect/Panel/Fullscreen/CheckFullscreen.pressed = config.readString("Settings", "Fullscreen", "False") == "True"
	$ColorRect/Panel/Relative/CheckRelative.pressed = config.readString("Settings", "RelativeMovement", "False") == "True"
	$ColorRect/Panel/BallSpeed/Speed.text = "Ball Speed: " + config.readString("Settings", "BallSpeed", "100")
	$ColorRect/Panel/PaddleSpeed/Speed.text = "Paddle Speed: " + config.readString("Settings", "PaddleSpeed", "50")
	$ColorRect/Panel/BallSpeed/SpeedSlider.value = int(config.readString("Settings", "BallSpeed", "100"))
	$ColorRect/Panel/PaddleSpeed/SpeedSlider.value = int(config.readString("Settings", "PaddleSpeed", "50"))

func applySettings():
	OS.window_fullscreen = config.readString("Settings", "Fullscreen", "False") == "True"

func saveSettingsAndClose():
	config.writeString("Settings", "Fullscreen", str($ColorRect/Panel/Fullscreen/CheckFullscreen.pressed))
	config.writeString("Settings", "RelativeMovement", str($ColorRect/Panel/Relative/CheckRelative.pressed))
	config.writeString("Settings", "BallSpeed", str($ColorRect/Panel/BallSpeed/SpeedSlider.value))
	config.writeString("Settings", "PaddleSpeed", str($ColorRect/Panel/PaddleSpeed/SpeedSlider.value))
	applySettings()
	
	events.emit_signal("settingsChanged")
	$Animation.play("Close")

func ballSpeedChanged(value: float):
	$ColorRect/Panel/BallSpeed/Speed.text = "Ball Speed: " + str(value)
	updateWarning()

func ballResetSpeed():
	$ColorRect/Panel/BallSpeed/Speed.text = "Ball Speed: 100"
	$ColorRect/Panel/BallSpeed/SpeedSlider.value = 100

func paddleSpeedChanged(value: float):
	$ColorRect/Panel/PaddleSpeed/Speed.text = "Paddle Speed: " + str(value)

func paddleResetSpeed():
	$ColorRect/Panel/PaddleSpeed/Speed.text = "Paddle Speed: 50"
	$ColorRect/Panel/PaddleSpeed/SpeedSlider.value = 50

func resetHighscore():
	$ColorRect/Panel/ResetConfirm.popup()

func resetScoreConfirm():
	config.writeString("Scores", "HighScore", "0")

func updateWarning():
	if $ColorRect/Panel/BallSpeed/SpeedSlider.value == 100:
		$ColorRect/Panel/SpeedWarning.visible = false
	else:
		$ColorRect/Panel/SpeedWarning.visible = true
