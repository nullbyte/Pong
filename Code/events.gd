extends Node

signal startGame
signal pauseGame
signal gameOver
signal hitPaddle
signal hitWall
signal settingsChanged
