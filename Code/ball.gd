extends KinematicBody2D

var velocity = Vector2.ZERO
var defaultspeed = int(config.readString("Settings", "Speed", "100")) * 10
var speed = 0
var edgevelocity = 200

func _ready():
	events.connect("startGame", self, "start")

func start():
	speed = defaultspeed
	position = get_viewport_rect().size / 2
	randomize()
	velocity.x = (randi() % 2) * speed
	if velocity.x == 0: velocity.x = -1 * speed
	randomize()
	velocity.y = (randi() % 2) * speed
	if velocity.y == 0: velocity.y = -1 * speed
	
	velocity = velocity.normalized() * (speed / 2)

func _physics_process(delta):
	var collision = move_and_collide(velocity * delta)
	if collision:
		if collision.collider.name == "Top" or collision.collider.name == "Bottom":
			velocity.y = -velocity.y
		elif collision.collider.name == "Right":
			events.emit_signal("hitWall")
			velocity.x = -velocity.x
		elif collision.collider.name == "Paddle":
			events.emit_signal("hitPaddle")
			speed += 30
			velocity.x = -velocity.x
			var distancefrommiddle = mapRange(collision.collider.position.y - position.y, -((collision.collider.get_child(1).shape.extents.y / 2) + 200), (collision.collider.get_child(1).shape.extents.y / 2) + 200, 1, -1)
			velocity.y =  distancefrommiddle * speed
			velocity = velocity.normalized() * (speed / 2)
			velocity += Vector2(-abs(distancefrommiddle * edgevelocity), distancefrommiddle * edgevelocity)
		elif collision.collider.name == "Left":
			events.emit_signal("gameOver")

func mapRange(value, leftMin, leftMax, rightMin, rightMax):
	var leftSpan = leftMax - leftMin
	var rightSpan = rightMax - rightMin
	var valueScaled = float(value - leftMin) / float(leftSpan)

	return rightMin + (valueScaled * rightSpan)
