extends Control

var action = 0

func _ready():
	visible = false
	events.connect("pauseGame", self, "showPaused")

func showPaused():
	visible = true
	get_tree().paused = true
	$Score.text = str(get_node("/root/Game").score)
	$AnimationPlayer.play("SlideUp")

func _on_animation_finished(anim_name):
	if anim_name == "SlideDown":
		visible = false
		if action == 0:
			get_tree().paused = false
		elif action == 1:
			events.emit_signal("startGame")
		elif action == 2:
			get_tree().change_scene("res://Scenes/mainmenu.tscn")

func resumeGame():
	action = 0
	$AnimationPlayer.play("SlideDown")

func restartGame():
	action = 1
	$AnimationPlayer.play("SlideDown")

func gotoMenu():
	action = 2
	$AnimationPlayer.play("SlideDown")
