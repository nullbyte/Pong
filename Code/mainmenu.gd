extends Control

func _ready():
	$Settings.visible = false
	get_tree().paused = false
	events.connect("settingsChanged", self, "updateScore")
	updateScore()

func start():
	get_tree().change_scene("res://Scenes/game.tscn")

func showSettings():
	$Settings.visible = true
	$Settings/Animation.play("Show")

func quitGame():
	get_tree().quit()

func updateScore():
	$HighScore.text = "High Score: " + config.readString("Scores", "HighScore", "0")
